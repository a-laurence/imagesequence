//img_sequence.hpp: Interface & implementation for extracting vide frames
// 
//////////////////////////////////////////////////////////////////////

#ifndef __IMG_SEQUENCE_HPP
#define __IMG_SEQUENCE_HPP

#include <opencv4/opencv2/core/core.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>
#include <opencv2/video.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <vector>
#include <string>
#include <iterator>

#include "logger.hpp"
#include "progress.hpp"

using namespace CPPLogging;

namespace ext
{
    inline void extractFrame(const std::string filepath, std::string destination, int& count)
    {
        LOG_DEBUG("Start frmae extraction for video file : %s", filepath.c_str());
        try
        {
            std::vector<int> compressionParams;
            compressionParams.push_back(cv::IMWRITE_JPEG_QUALITY);

            cv::VideoCapture cap(filepath);
            if (!cap.isOpened())
                LOG_ERROR("Could not load vide file: %s", filepath.c_str());
            
            Progress bar;
            for (int f = 0; f < cap.get(cv::CAP_PROP_FRAME_COUNT); f++)
            {
                LOG_DEBUG("cap[%i] Extractig frame from the video", f);
                cv::Mat frame;
                cap >> frame;

                std::string sequence = "000000000";
                sequence.append(std::to_string(f));
                if (sequence.length() > 10) sequence.erase(2, sequence.length() - 10);
                std::string filename = destination + "/" + sequence + ".jpg";
                cv::imwrite(filename, frame, compressionParams);
                bar.update((static_cast<double>(f+1) / static_cast<double>(cap.get(cv::CAP_PROP_FRAME_COUNT))) * 100);
                bar.setStatusText("[" + std::to_string(count) + "]");
            }
        }
        catch(const std::exception& e)
        {
            LOG_ERROR("Error: %s", e.what());
            exit(1);
        }
    }

#endif // !__IMG_SEQUENCE_HPP
