//logger.hpp Interface for logger class
// 
//////////////////////////////////////////////////////////////////////

#ifndef __LOGGER_HPP
#define __LOGGER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <errno.h>
#include <pthread.h>

namespace CPPLogging
{
    #define LOG_ERROR(...)        Logger::getInstance() -> error(strrchr(__FILE__, '/') + 1, __FUNCTION__, log(__VA_ARGS__))
    #define LOG_ALWAYS(...)       Logger::getInstance() -> always(strrchr(__FILE__, '/') + 1, __FUNCTION__, log(__VA_ARGS__))
    #define LOG_INFO(...)         Logger::getInstance() -> info(strrchr(__FILE__, '/') + 1, __FUNCTION__, log(__VA_ARGS__))
    #define LOG_TRACE(...)        Logger::getInstance() -> trace(strrchr(__FILE__, '/') + 1, __FUNCTION__, log(__VA_ARGS__))
    #define LOG_DEBUG(...)        Logger::getInstance() -> debug(strrchr(__FILE__, '/') + 1, __FUNCTION__, log(__VA_ARGS__))
    #define LOG_BUFFER(...)       Logger::getInstance() -> buffer(log(__VA_ARGS__))

    typedef enum LOG_LEVEL
    {
        DISABLE_LOG = 1,
        LOG_LEVEL_INFO = 2,
        LOG_LEVEL_BUFFER = 3,
        LOG_LEVEL_TRACE = 4,
        LOG_LEVEL_DEBUG = 5,
        ENABLE_LOG = 6,logging
    }LogLevel;

    typedef enum LOG_TYPE
    {
        NO_LOG = 1,
        FILE_LOG = 2,
    }LogType;

    class Logger
    {
    public:
        static Logger* getInstance() throw();

        void error(std::string f, std::string fn, const char* text) throw();
        void error(std::string f, std::string fn, std::string text) throw();
        void error(std::string f, std::string fn, std::ostringstream& stream) throw();

        void always(std::string f, std::string fn, const char* text) throw();
        void always(std::string f, std::string fn, std::string text) throw();
        void always(std::string f, std::string fn, std::ostringstream& stream) throw();

        void info(std::string f, std::string fn, const char* text) throw();
        void info(std::string f, std::string fn, std::string text) throw();
        void info(std::string f, std::string fn, std::ostringstream& stream) throw();

        void trace(std::string f, std::string fn, const char* text) throw();
        void trace(std::string f, std::string fn, std::string text) throw();
        void trace(std::string f, std::string fn, std::ostringstream& stream) throw();

        void debug(std::string f, std::string fn, const char* text) throw();
        void debug(std::string f, std::string fn, std::string text) throw();
        void debug(std::string f, std::string fn, std::ostringstream& stream) throw();

        void buffer(const char* text) throw();
        void buffer(std::string text) throw();
        void buffer(std::ostringstream& stream) throw();

        void updateLogLevel(LogLevel logLevel);
        void enableLog();
        void disableLog();

        void updateLogType(LogType logType);
        void enableFileLogging();

    protected:
        Logger();
        ~Logger();
        void lock();
        void unlock();

    private:
        void logIntoFile(std::string data);
        // Logger(const Logger& obj) {}
        // void operator=(const Logger& obj) {}

    private:
        static Logger* m_Instance;
        std::ofstream m_File;
        pthread_mutexattr_t m_Attr;
        pthread_mutex_t m_Mutex;
        LogLevel m_LogLevel;
        LogType m_LogType;
    };
} // End of namespace

class LogFile
{
    const std::string FILE_PREFIX = "log-";
    const std::string FILE_SUFFIX = ".log";

public:
    LogFile() {}
    std::string getFilename() const;
};

std::string getCurrentTime(std::string s);
std::string log(const char* fmt, ...);

#endif // !__LOGGER_HPP
