// maincpp: Defines the entry point of the application
//
//////////////////////////////////////////////////////////////////////

#include <opencv4/opencv2/core/core.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>
#include <filesystem>
#include <iostream>
#include <vector>
#include <string>
#include <future>
#include <thread>
#include <chrono>
#include "logger.hpp"
#include "img_sequence.hpp"

using namespace CPPLogging;
namespace fs = std::filesystem;

int main()
{
    LOG_ALWAYS("START OF PROGRAM");

    std::string cwd = fs::current_path();
    std::vector<std::string> videos;
    std::vector<std::future<void>> futures;

    int count = 0;
    for (auto& video : fs::directory_iterator(cwd + "/data/source/"))
    {
        count += 1;
        
        std::string destination = cwd + "/data/output/";
        destination.append(fs::path(video).filename());
        destination.erase(destination.length() - 4, 4);
        fs::create_directories(destination);

        ext::extractFrame(video.path(), destination, count);
    }
    
    LOG_BUFFER("Done!");
    LOG_ALWAYS("END OF PROGRAM");
}
