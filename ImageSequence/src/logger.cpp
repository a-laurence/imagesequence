//logger.cpp: Implementation for logger class
// 
//////////////////////////////////////////////////////////////////////

#include "logger.hpp"

#include <filesystem>
#include <iostream>
#include <ctime>
#include <fstream>
#include <string>
#include <stdarg.h>

using namespace CPPLogging;
namespace fs = std::filesystem;

Logger* Logger::m_Instance = 0;
LogFile lgf;

const std::string logFileName = lgf.getFilename();

Logger::Logger()
{
    m_File.open(logFileName.c_str(), std::ios::out | std::ios::app);
    m_LogLevel = LOG_LEVEL_TRACE;
    m_LogType = FILE_LOG;
    int ret = 0;
    ret = pthread_mutexattr_settype(&m_Attr, PTHREAD_MUTEX_ERRORCHECK_NP);
    if(ret != 0)
    {
        std::cout << "Logger::Logger() -- Mutex attribute not initialize!!\n";
        exit(0);
    }
    ret = pthread_mutex_init(&m_Mutex, &m_Attr);
    if(ret != 0)
    {
        std::cout << "Logger::Logger() -- Mutex not initialize!!\n";
        exit(0);
    }
}

Logger::~Logger()
{
    m_File.close();
    pthread_mutexattr_destroy(&m_Attr);
    pthread_mutex_destroy(&m_Mutex);
}

Logger* Logger::getInstance() throw()
{
    if(m_Instance ==0) m_Instance = new Logger();
    return m_Instance;
}

void Logger::lock() {pthread_mutex_lock(&m_Mutex);}
void Logger::unlock() {pthread_mutex_unlock(&m_Mutex);}

void Logger::logIntoFile(std::string __data)
{
    std::string now = getCurrentTime("log");
    lock();
    m_File << now << " " << __data << std::endl;
    unlock();

    // std::cout << now << " " << __data << std::endl;    
}

void Logger::error(std::string f, std::string fn, const char* text) throw()
{
    std::ostringstream data;
    data << "[ERROR\t]" << "[" << f << "\t]" << "[" << fn << "\t]" << text;
    logIntoFile(data.str());
}
void Logger::error(std::string f, std::string fn, std::string text) throw()
{
    error(f, fn, text.data());
}
void Logger::error(std::string f, std::string fn, std::ostringstream& stream) throw()
{
    std::string text = stream.str();
    error(f, fn, text.data());
}

void Logger::trace(std::string f, std::string fn, const char* text) throw()
{
    std::ostringstream data;
    data << "[TRACE\t]" << "[" << f << "\t]" << "[" << fn << "\t]" << text;
    logIntoFile(data.str());
}
void Logger::trace(std::string f, std::string fn, std::string text) throw()
{
    trace(f, fn, text.data());
}
void Logger::trace(std::string f, std::string fn, std::ostringstream& stream) throw()
{
    std::string text = stream.str();
    trace(f, fn, text.data());
}

void Logger::always(std::string f, std::string fn, const char* text) throw()
{
    std::ostringstream data;
    data << "[ALWAYS\t]" << "[" << f << "\t]" << "[" << fn << "\t]" << text;
    logIntoFile(data.str());
}
void Logger::always(std::string f, std::string fn, std::string text) throw()
{
    always(f, fn, text.data());
}
void Logger::always(std::string f, std::string fn, std::ostringstream& stream) throw()
{
    std::string text = stream.str();
    always(f, fn, text.data());
}

void Logger::info(std::string f, std::string fn, const char* text) throw()
{
    std::ostringstream data;
    data << "[INFO\t]" << "[" << f << "\t]" << "[" << fn << "\t]" << text;
    logIntoFile(data.str());
}
void Logger::info(std::string f, std::string fn, std::string text) throw()
{
    info(f, fn, text.data());
}
void Logger::info(std::string f, std::string fn, std::ostringstream& stream) throw()
{
    std::string text = stream.str();
    info(f, fn, text.data());
}

void Logger::debug(std::string f, std::string fn, const char* text) throw()
{
    std::ostringstream data;
    data << "[DEBUG\t]" << "[" << f << "\t]" << "[" << fn << "\t]" << text;
    logIntoFile(data.str());
}
void Logger::debug(std::string f, std::string fn, std::string text) throw()
{
    debug(f, fn, text.data());
}
void Logger::debug(std::string f, std::string fn, std::ostringstream& stream) throw()
{
    std::string text = stream.str();
    debug(f, fn, text.data());
}

void Logger::buffer(const char* text) throw()
{
    std::cout << text << std::endl;
}

void Logger::buffer(std::string text) throw()
{
    buffer(text.data());
}

void Logger::updateLogLevel(LogLevel logLevel) {m_LogLevel = logLevel;}
void Logger::enableLog() {m_LogLevel = ENABLE_LOG;}
void Logger::disableLog() {m_LogLevel = DISABLE_LOG;}
void Logger::updateLogType(LogType logType) {m_LogType = logType;}
void Logger::enableFileLogging() {m_LogType = FILE_LOG;}
 
std::string LogFile::getFilename() const
{
    if (fs::exists("logs/"))
        fs::remove_all("logs/");
    
    fs::create_directories("logs/");
    std::string logfile = "logs/logfile.log";

    std::ofstream __f;
    __f.open(logfile);
    __f << "\n Project Name:\tImage Sequence";
    __f << "\n Version:\t\t0.01";
    __f << "\n Date & Time:\t" << getCurrentTime("log") << "\n\n";
    __f.close();
    return logfile;
}

std::string getCurrentTime(std::string s)
{
    time_t __now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&__now);
    if(s=="file") strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);
    else if(s=="log") strftime(buf, sizeof(buf), "%Y-%m-%d-%X", &tstruct);
    else if(s=="img") strftime(buf, sizeof(buf), "%Y%m%d%H%M%S", &tstruct);
    return std::string(buf);
}

std::string log(const char* fmt, ...)
{
    std::string result = "[\t]";
    std::ostringstream stream;
    va_list args;
    va_start(args, fmt);

    while(*fmt != '\0')
    {
        if(*fmt == '\\')
        {
            fmt++;
            stream << fmt;
        }
        else if (*fmt == '%')
        {
            fmt++;
            switch(*fmt)
            {
                case 'i':
                {
                    int i = va_arg(args, int);
                    stream << i;
                    break;
                }
                case 'c':
                {
                    int c = va_arg(args, int);
                stream << static_cast<char>(c);
                break;
                }
                case 'f':
                {
                    double f = va_arg(args, double);
                    stream << f;
                    break;
                }
                case 's':
                {
                    char* s = va_arg(args, char*);
                    stream << s;
                    break;
                }
                default:
                {
                    stream << *fmt;
                    break;
                }
            }
        }
        else stream << static_cast<char>(*fmt);
        ++fmt;
    }
    va_end(args);
    return stream.str();
}
